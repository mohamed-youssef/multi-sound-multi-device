import os
import threading
from os import listdir
from os.path import isfile, join

import numpy
import sounddevice
import soundfile
from playsound import playsound
from scipy.io.wavfile import read

import gi

DATA_TYPE = "float32"

def load_sound_file_into_memory(path):
    """
    Get the in-memory version of a given path to a wav file
    :param path: wav file to be loaded
    :return: audio_data, a 2D numpy array
    """

    audio_data, _ = soundfile.read(path, dtype=DATA_TYPE)
    return audio_data



def get_device_number_if_usb_soundcard(index_info):
    """
    Given a device dict, return True if the device is one of our USB sound cards and False if otherwise
    :param index_info: a device info dict from PyAudio.
    :return: True if usb sound card, False if otherwise
    """
 
    index, info = index_info

    if info['max_output_channels'] > 0:
        return index
    return False


def create_running_output_stream(index):
    """
    Create an sounddevice.OutputStream that writes to the device specified by index that is ready to be written to.
    You can immediately call `write` on this object with data and it will play on the device.
    :param index: the device index of the audio device to write to
    :return: a started sounddevice.OutputStream object ready to be written to
    """
    try:
        output = sounddevice.OutputStream(
            device=index,
            dtype=DATA_TYPE
        )
        output.start()
        return output
    except:
        pass


def play_wav_on_index(audio_data, usb_sound_card_indices):
    """
    Play an audio file given as the result of `load_sound_file_into_memory`
    :param audio_data: A two-dimensional NumPy array
    :param stream_object: a sounddevice.OutputStream object that will immediately start playing any data written to it.
    :return: None, returns when the data has all been consumed
    """
    LONG_DIR = os.path.join(os.getcwd(), 'sounds', 'long')
    SHORT_DIR = os.path.join(os.getcwd(), 'sounds', 'short')
    devices = sounddevice.query_devices()
    print(devices)
    print(usb_sound_card_indices)
    for file in os.listdir(LONG_DIR):
        file_path = os.path.join(LONG_DIR, file)
        data, samplerate = soundfile.read(file_path, dtype='float32')
        sounddevice.play(data, samplerate, device=device)
        # for device in usb_sound_card_indices:


    # for file in os.listdir(SHORT_DIR):
    #     file_path = os.path.join(SHORT_DIR, file)
    #     data, samplerate = soundfile.read(file_path, dtype='float32')
    #     for device in usb_sound_card_indices:
    #         sounddevice.play(data, samplerate, device=1)
        
    # stream_object.write(audio_data)s


if __name__ == "__main__":

    # Sound files paths
    long_dir = os.path.join(os.getcwd(), 'sounds', 'long')
    short_dir = os.path.join(os.getcwd(), 'sounds', 'short')

    #list all sound files
    long_sound_files=[os.path.join(long_dir, f) for f in listdir(long_dir) if isfile(join(os.path.join(long_dir, f)))]
    short_sound_files = [os.path.join(short_dir, f) for f in listdir(short_dir) if isfile(join(os.path.join(short_dir, f)))]


    sound_file_paths = long_sound_files + short_sound_files
    # print(sound_file_paths)
    
    files = [load_sound_file_into_memory(path) for path in sound_file_paths]
    # print(files)


    usb_sound_card_indices =  list(filter(lambda x: x is not False,
                                         map(get_device_number_if_usb_soundcard,
                                             [index_info for index_info in enumerate(sounddevice.query_devices())])))

    print("Discovered the following usb sound devices", usb_sound_card_indices)
                            

    
    
    
    streams = [create_running_output_stream(i) for i in usb_sound_card_indices]
    streams = [stream for stream in streams if stream is not None]
    
    running = True

    if not len(streams) > 0:
        running = False
        print("No audio devices found, stopping")
 
    if not len(files) > 0:
        running = False
        print("No sound files found, stopping")
 
    while running:
 
        print("Playing files")

        threads = [threading.Thread(target=play_wav_on_index, args=[file_path, usb_sound_card_indices])
                   for file_path, stream in zip(files, streams)] 
        # try:
 
        #     for thread in threads:
        #         thread.start()
         
 
        #     for thread, device_index in zip(threads, usb_sound_card_indices):
        #         print("Waiting for device", device_index, "to finish")
        #         thread.join()
 
        # except KeyboardInterrupt:
        #     running = False
        #     print("Stopping stream")
        #     for stream in streams:
        #         stream.abort(ignore_errors=True)
        #         stream.close()
        #     print("Streams stopped")
 
    print("Bye.")
