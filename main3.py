import os
import sounddevice as sd
import soundfile as sf


LONG_DIR = os.path.join(os.getcwd(), 'sounds', 'long')
SHORT_DIR = os.path.join(os.getcwd(), 'sounds', 'short')


for file in os.listdir(LONG_DIR):
    file_path = os.path.join(LONG_DIR, file)
    data, samplerate = sf.read(file_path, dtype='float32')
    # print(data,samplerate)

    sd.play(data, samplerate, device=5)
    # for device in sd.query_devices():
        # sd.play(data, samplerate, device=8)
        # print(device)
        # if device['max_output_channels'] > 0:
        #     print(device)


        # [2] Microsoft Sound Mapper - Output, MME
        # [4] Realtek Digital Output (Realtek, MME
        # [9] Realtek Digital Output (Realtek High Definition Audio), Windows DirectSound
        # [11]
        # [14]

devices = sd.query_devices(5)
# print(devices)